import {Request, Response, Router} from "express";
import {UserService} from "../service/user.service";
import {requestValidator} from "../middleware/validator.middleware";
import {UserObjectValidator} from "../validator/userObject.validator";
import {ParamsObjectValidator} from "../validator/paramsObject.validator";

export const userRouter = Router();

const userService = UserService.getInstance();

userRouter.route("/")
    .get((req: Request, res: Response) => {
        res.send(userService.getUsers());
    })
    .post(requestValidator("body", UserObjectValidator.shouldBeValidPostUserDTO),
        (req: Request, res: Response) => {
        res.send(userService.addUser(req.body));
    })
    .delete((req: Request, res: Response) => {
        userService.deleteAll();
        res.send({message: "Success"});
    });

userRouter.route("/:id")
    .get(requestValidator("params", ParamsObjectValidator.shouldBeValidId),
        (req: Request, res: Response) => {
        res.send(userService.getUserById(Number(req.params.id)));
    })
    .patch(requestValidator("params", ParamsObjectValidator.shouldBeValidId),
        requestValidator("body", UserObjectValidator.shouldBeValidPatchUserDTO),
        (req: Request, res: Response) => {
        res.send(userService.updateUser(Number(req.params.id), req.body));
    })
    .delete(requestValidator("params", ParamsObjectValidator.shouldBeValidId),
        (req: Request, res: Response) => {
        userService.deleteUserById(Number(req.params.id));
        res.send({message: "Success"});
    });
