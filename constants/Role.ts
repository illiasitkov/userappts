
export enum Role {
    ADMIN = "ADMIN",
    SALESMAN = "SALESMAN",
    CUSTOMER = "CUSTOMER"
}