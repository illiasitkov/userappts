import {BadRequestError} from "./badRequestError.model";

export class ValidationError extends BadRequestError {
    constructor(message: string) {
        super(message);
    }
}

