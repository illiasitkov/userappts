import {Role} from "../constants/Role";

export interface User {
    username: string;
    role: Role;
    id: number;
    address: string;
    email: string;
}
