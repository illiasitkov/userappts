import {PropertyValidator} from "./property.validator";
import {Role} from "../constants/Role";
import {UserUpdate} from "../model/userUpdate.model";
import {ObjectValidationFunction} from "./objectValidationFunctionType";

export class UserObjectValidator {

    static shouldBeValidPostUserDTO: ObjectValidationFunction = (userPostDto: any): void => {
        PropertyValidator
            .validate("username", userPostDto.username)
            .shouldBeIdentifier()
            .shouldNotBeBlank();

        PropertyValidator
            .validate("email", userPostDto.email)
            .shouldBeEmail()
            .shouldNotBeBlank();

        PropertyValidator
            .validate("role", userPostDto.role)
            .shouldBeOneOf([Role.ADMIN, Role.CUSTOMER, Role.SALESMAN])
            .shouldNotBeBlank();

        PropertyValidator
            .validate("id", userPostDto.id)
            .shouldBeNumeric();

        PropertyValidator
            .validate("address", userPostDto.address)
            .shouldBeAlphanumeric(true)
            .shouldNotBeBlank();
    }

    static shouldBeValidPatchUserDTO: ObjectValidationFunction = (userUpdate: UserUpdate): void => {
        PropertyValidator
            .validate("username", userUpdate.username)
            .shouldBeIdentifier();

        PropertyValidator
            .validate("address", userUpdate.address)
            .shouldBeAlphanumeric(true);
    }

}





