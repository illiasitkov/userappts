import {ValidationError} from "../model/validationError.model";

export class PropertyValidator {


    private constructor(private propertyName: string, private propertyValue: any) {
    }

    static validate = (propertyName: string, propertyValue: any): PropertyValidator => {
        return new PropertyValidator(propertyName, propertyValue);
    }

    shouldBeEmail = (): PropertyValidator  => {
        if (this.isBlank()) {
            return this;
        }
        const regex = new RegExp("^(\\w+\.?)*\\w+@(\\w{2,5}\.)+\\w{2,5}$");
        const message = `Value of the property '${this.propertyName}' should be a properly constructed email`;
        this.throwIfFalse(regex.test(this.propertyValue), new ValidationError(message));
        return this;
    }

    shouldBeIdentifier = (): PropertyValidator => {
        if (this.isBlank()) {
            return this;
        }
        const regex = new RegExp("^\\w+$");
        const message = `Value of the property '${this.propertyName}' should contain only a-z, A-Z, 0-9, _ characters`;
        this.throwIfFalse(regex.test(this.propertyValue), new ValidationError(message));
        return this;
    }

    shouldBeNumeric = (): PropertyValidator => {
        if (this.isBlank()) {
            return this;
        }
        const regex = new RegExp("^\\d+$");
        const message = `Value of the property '${this.propertyName}' should contain only 0-9 characters`;
        this.throwIfFalse(regex.test(this.propertyValue), new ValidationError(message));
        return this;
    }

    shouldBeAlphanumeric = (allowSpaces: boolean): PropertyValidator => {
        if (this.isBlank()) {
            return this;
        }
        let regex;
        if (allowSpaces) {
            regex = new RegExp("^[\\da-zA-Z\\s]+$");
        } else {
            regex = new RegExp("^[\\da-zA-Z]+$");
        }
        const message = `Value of the property '${this.propertyName}' should contain only alphanumeric characters${allowSpaces ? " and whitespaces" : ""}`;
        this.throwIfFalse(regex.test(this.propertyValue), new ValidationError(message));
        return this;
    }

    shouldNotBeBlank = (): PropertyValidator => {
        const message = `Value of the property '${this.propertyName}' should not be blank`;
        this.throwIfFalse(!this.isBlank(), new ValidationError(message));
        return this;
    }

    shouldBeOneOf = (allowedValues: any[]): PropertyValidator => {
        if (this.isBlank()) {
            return this;
        }
        const message = `Value of the property '${this.propertyName}' should be one of ${allowedValues}`;
        const contains = allowedValues.find((v: any) => v === this.propertyValue);
        this.throwIfFalse(contains, new ValidationError(message));
        return this;
    }

    private throwIfFalse = (condition: boolean, error: Error): void => {
        if (!condition) {
            throw error;
        }
    }

    private isBlank = (): boolean => {
        return !this.propertyValue || this.propertyValue?.toString().trim().length <= 0;
    }

}




