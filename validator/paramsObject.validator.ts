import {ObjectValidationFunction} from "./objectValidationFunctionType";
import {PropertyValidator} from "./property.validator";


export class ParamsObjectValidator {

    static shouldBeValidId: ObjectValidationFunction = (object: any): void => {
        PropertyValidator.validate("id", object.id)
            .shouldBeNumeric()
            .shouldNotBeBlank();
    }


}



