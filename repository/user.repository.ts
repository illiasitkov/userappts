import {User} from "../model/user.model";
import {generateId} from "../util";
import {NotFoundError} from "../model/notFoundError.model";
import {UserUpdate} from "../model/userUpdate.model";

export class UserRepository {

    private static instance: UserRepository;

    private users: User[] = [];

    private constructor() {}

    static getInstance(): UserRepository {
        if (!UserRepository.instance) {
            UserRepository.instance = new UserRepository();
        }
        return UserRepository.instance;
    }

    createUser = (user: User): User => {
        user.id = generateId();
        this.users.push(user);
        return user;
    };

    updateUser = (id: number, userUpdate: UserUpdate): User => {
        const user: User = this.getUserById(id);
        if (!user) {
            throw new NotFoundError(`User with id = ${id} bot found`);
        }
        let p: keyof UserUpdate;
        for (p in userUpdate) {
            if (userUpdate[p]) {
                user[p] = userUpdate[p];
            }
        }
        return user;
    };

    getUserById = (id: number): User => {
        const user: User | undefined = this.users.find(u => u.id === id);
        if (!user) {
            throw new NotFoundError(`User with id = ${id} bot found`);
        }
        return user;
    };

    deleteUserById = (id: number): void => {
        this.getUserById(id);
        this.users = this.users.filter(u => u.id !== id);
    };

    deleteAll = (): void => {
        this.users = [];
    };

    getUsers = (): User[] => {
        return this.users;
    };

}

