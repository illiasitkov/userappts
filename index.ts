import express, { Express } from 'express';
import dotenv from 'dotenv';
import {userRouter} from "./route/user.router";
import {errorHandler} from "./middleware/errorHandler.middleware";

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 8080;

app.use(express.json());

app.use("/api/users", userRouter);
app.use(errorHandler);

app.listen(port, () => {
    console.log(`️Server is running at http://localhost:${port}`);
});
