import {UserRepository} from "../repository/user.repository";
import {User} from "../model/user.model";
import {BadRequestError} from "../model/badRequestError.model";
import {UserUpdate} from "../model/userUpdate.model";

export class UserService {

    private static instance: UserService;

    private userRepository: UserRepository;

    private constructor() {
        this.userRepository = UserRepository.getInstance();
    }

    static getInstance(): UserService {
        if (!UserService.instance) {
            UserService.instance = new UserService();
        }
        return UserService.instance;
    }

    addUser = (user: User): User => {
        this.propertyShouldBeUnique("username", user.username);
        this.propertyShouldBeUnique("email", user.email);
        return this.userRepository.createUser(user);
    }

    updateUser = (id: number, userUpdate: UserUpdate): User => {
        this.propertyShouldBeUnique("username", userUpdate.username, id);
        return this.userRepository.updateUser(id, userUpdate);
    }

    getUserById = (id: number): User => {
        return this.userRepository.getUserById(id);
    }

    getUsers = (): User[] => {
        return this.userRepository.getUsers();
    }

    deleteUserById = (id: number): void => {
        this.userRepository.deleteUserById(id);
    }

    deleteAll = (): void => {
        this.userRepository.deleteAll();
    }

    private propertyShouldBeUnique = (propertyName: string, value: string, id?: number): void => {
        if (this.userRepository.getUsers().find(u => u.id !== id && u[propertyName as keyof User] === value)) {
            throw new BadRequestError(`Value for property '${propertyName}' should be unique`);
        }
    }

}











