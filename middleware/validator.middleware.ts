import {ObjectValidationFunction} from "../validator/objectValidationFunctionType";
import {NextFunction, Request, Response} from "express";

export const requestValidator = (requestPropertyName: string, validationFunction: ObjectValidationFunction) =>
    (req: Request, res: Response, next: NextFunction) => {
    try {
        validationFunction(req[requestPropertyName as keyof Request]);
        next();
    } catch (e) {
        next(e);
    }
}



