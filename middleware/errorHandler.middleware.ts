import {NextFunction, Request, Response} from "express";
import {NotFoundError} from "../model/notFoundError.model";
import {BadRequestError} from "../model/badRequestError.model";
import {ErrorResponse} from "../model/errorResponse.model";

export const errorHandler = (error: Error, req: Request, res: Response, next: NextFunction): void => {
    let statusCode;
    if (error instanceof NotFoundError) {
        statusCode = 404;
    } else if (error instanceof BadRequestError) {
        statusCode = 400;
    } else {
        statusCode = 500;
    }
    res.status(statusCode);
    res.send(new ErrorResponse(error.message));
}
